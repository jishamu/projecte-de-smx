# Per servidor
## openvpn-install
OpenVPN [road warrior](http://en.wikipedia.org/wiki/Road_warrior_%28computing%29) instal·lador per a Ubuntu, Debian, CentOS i Fedora.

Aquest script us permetrà configurar el vostre propi servidor VPN en no més d’un minut, fins i tot si abans no heu utilitzat OpenVPN. Ha estat dissenyat perquè sigui el més discret i universal possible.


## Instal·lació
Executeu el següent ordre per install OpenVPN:
`apt-get install openvpn`

Executeu el script seguiu l’assistent:
`wget https://git.io/vpn -O openvpn-install.sh && bash openvpn-install.sh`

Un cop finalitzeu, podeu tornar a executar-lo per afegir més usuaris, eliminar-ne alguns o fins i tot desinstal·lar completament OpenVPN.

# Per client
## Instal·lació
Executeu el següent ordre per install OpenVPN:
`apt-get install openvpn`

## Per connectar-se
Executeu el següent ordre per poder connectar al server:
`open openvpn jiaooscarClient.ovpn`
